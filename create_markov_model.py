import math, os, re
from collections import deque
import cPickle as pickle
c_count = 'count'
text = ''

start_char = '^'
stop_char = '$'

for dirname, dirnames, filenames in os.walk('input_files'):
  for filename in filenames:
    with open(os.path.join(dirname,filename)) as f:
      text += ' '+f.read()

raw_words = text.split()
p=re.compile('[^a-zA-Z]')
p_words = []
for word in raw_words:
  newword=p.sub('',word).lower()
  if len(newword) != 0:
    p_words.append(newword)

n_graph_matrices = {}
word_dict = set(p_words)
def generate_n_graphs(n):
  n_graph_matrices[n] = {}
  for word in p_words:
    # use a queue with n places to store our n-graphs. When it's full, adding
    # a new element removes the oldest element.
    letter_queue = deque([],n)
    # prefix the word with enough start characters to form an n-gram with only
    # the first letter.
    for letter in start_char*n + word + stop_char:
      # Check we've read enough letters to have an n-graph in the queue
      if len(letter_queue) == n:
        n_graph = ''
        # Build our n-graph from the queue
        for i in letter_queue:
          n_graph += i
        # update the count for this n-graph
        if n_graph not in n_graph_matrices[n]:
          n_graph_matrices[n][n_graph] = {}
          n_graph_matrices[n][n_graph][c_count] = 0
        n_graph_matrices[n][n_graph][c_count] += 1
        # update the count for the next letter after this n-graph
        if letter not in n_graph_matrices[n][n_graph]:
          n_graph_matrices[n][n_graph][letter] = 0
        n_graph_matrices[n][n_graph][letter] += 1
      # put the next letter on the queue, forming the next n-gram
      letter_queue.append(letter)
      
for i in range(5):
  print 'Generating model for '+str(i+1)+'-graphs'
  generate_n_graphs(i+1)
  print 'Completed model for '+str(i+1)+'-graphs'
  

pickle.dump(n_graph_matrices, open('model.p','wb'),2)
pickle.dump(word_dict, open('words.p','wb'),2)


