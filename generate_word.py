import math, random, cgitb
import cPickle as pickle
from collections import deque

keyboard_halves = ['qwertasdfgzxcv','yuiophjklbnm']

start_char = '^'
stop_char = '$'
c_count = 'count'
# Unpickle the models
n_graph_matrices = pickle.load(open('model.p','rb'))
word_dict = pickle.load(open('words.p','rb'))

def generate_word(n):
  if n not in n_graph_matrices:
    print 'ERROR: Model for '+str(n)+'-graphs was not found'
    return
  letter_queue = deque(start_char*n,n)
  word=start_char
  while word[-1] != stop_char:
    n_graph=''
    for i in letter_queue:
      n_graph += i
    count = 0
    random_number = math.trunc(n_graph_matrices[n][n_graph][c_count] * random.random())
    for candidate_letter in n_graph_matrices[n][n_graph]:
      if candidate_letter <> c_count:
        count += n_graph_matrices[n][n_graph][candidate_letter]
        if random_number<count:
          letter = candidate_letter
          break
    letter_queue.append(letter)
    word += letter
  return word[1:-1]
  
def generate_new_word(n,minlen):
  word=generate_word(n)
  attempts = 0
  max_attempts = 500
  while ( len(word) < minlen or word in word_dict ) and attempts < max_attempts:
    attempts += 1
    word = generate_word(n)
  if attempts >= max_attempts:
    print 'max iterations exceeded'
  return word
  
def is_alternating(word):
  alternater = 0
  if word[0] not in keyboard_halves[alternater]:
    alternater = 1 - alternater
  for i in word:
    if i not in keyboard_halves[alternater]:
      return False
    alternater = 1 - alternater
  return True
  
def generate_alternating_word(n, minlen):
  word = generate_new_word(n, minlen)
  attempts = 0
  max_attempts = 500
  while not is_alternating(word) and attempts < max_attempts:
    attempts += 1
    word = generate_new_word(n, minlen)
  if attempts >= max_attempts:
    print 'max iterations exceeded'
  return word
  
def word_is_very_new(word):
  for i in word_dict:
    if i in word and len(i) >= round(len(word)/2.0):
      return False
  return True
  
def generate_very_new_word(n,minlen):
  word = generate_new_word(n,minlen)
  attempts = 0
  max_attempts = 500
  while not word_is_very_new(word) and attempts < max_attempts:
    attempts += 1
    word = generate_new_word(n,minlen)
  if attempts >= max_attempts:
    print 'max iterations exceeded'
  return word
  
print generate_very_new_word(2,8)
